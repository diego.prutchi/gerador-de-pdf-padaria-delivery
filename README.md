# Gerador de PDF - Padaria Delivery

Sistema feito como primeiro desafio técnico do Bootcamp de Full Stack do Instituto Infnet.
## APIs

Não foi gerada nenhuma API interna.

São consumidas 2 APIs externas:

-ViaCEP

-Google Maps


## Autor

- [Diego Prutchi](https://linkedin.com/in/diegoprutchi)

  
## Live Demo
https://projeto-1-padaria-delivery.herokuapp.com/



  
## Funcionalidades

- Formulário dinâmico baseado em Models
- Endereço preenchido através do CEP
- Geração de PDF com mapa das redondezas baseado no CEP

  
## Feedback

Será muito apreciado. Por favor envie para diego.prutchi@al.infnet.edu.br


  
## Execute o projeto localmente

Clone o projeto

```bash
  git clone https://gitlab.com/diego.prutchi/gerador-de-pdf-padaria-delivery.git
```

Vá para a pasta do projeto

```bash
  cd <nome_pasta>
```

Instale as dependências

```bash
  npm install
```

Inicie o Servidor

```bash
  npm run start
```

  
