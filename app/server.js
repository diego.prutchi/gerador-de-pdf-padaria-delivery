const express = require("express");
const servidor = express();
const path = require("path");
const bp = require("body-parser");

const rotas = require("./routes/rotas");

servidor.use(bp.json());
servidor.use(bp.urlencoded());

rotas(servidor);

servidor.use(express.static(path.join(__dirname, "public"))); // apontar pasta publica para rodar os assets ("estoque")

servidor.set('views', path.join(__dirname, "views")) // informar o express onde está a pasta view
servidor.set("view engine", "ejs"); // escolher o template enginner para rodar o "html" do projeto




const porta = process.env.PORT || 5000;
servidor.listen(porta, () => {
  console.log("SERVIDOR RODANDO NA PORTA ", porta);
});

//no caso do https o query trafega encrpitado?
