const TabelaDB = [
  { id: "prod1", valor: "valor1", preco: 0.5, produto: "Pão Francês", unidade: " Un." },
  { id: "prod2", valor: "valor2", preco: 8.5, produto: "Pão de Forma", unidade: " Pct." },
  { id: "prod3", valor: "valor3", preco: 4.9, produto: "Queijo Mussarela", unidade: " x 100g" },
  { id: "prod4", valor: "valor4", preco: 5.2, produto: "Queijo Prato", unidade: " x 100g" },
  { id: "prod5", valor: "valor5", preco: 7.2, produto: "Requeijão", unidade: " Un." },
  { id: "prod6", valor: "valor6", preco: 3.5, produto: "Manteiga barra", unidade: " Un." },
  { id: "prod7", valor: "valor7", preco: 4.9, produto: "Pão de Queijo", unidade: " x 100g" },
  { id: "prod8", valor: "valor8", preco: 3.7, produto: "Leite integral", unidade: " Lt." },
  { id: "prod9", valor: "valor9", preco: 3.3, produto: "Leite desnatado", unidade: " Lt." },
  { id: "prod10", valor: "valor10", preco: 7.0, produto: "Coca-Cola", unidade: " Lt." }
]

let total = 0;

function verifica() {

  let total = 0;
  let talher = 0;
  let listageral = [];

  for (let i = 1; i < 11; i++) {
    let valor = document.getElementById("prod" + i).value;

    const preco = TabelaDB.find(item => item.id == ("prod" + i))

    let valortxt = ""
    if (valor == 0) {
      valortxt = "R$ 0.00"
    } else {
      valortxt = "R$ " + (parseFloat((valor * preco.preco)).toFixed(2))
      // document.getElementById("lista" + i).value = (preco.produto + "  " + valor + preco.unidade + " Total: " + valortxt)  
      listageral.push({ "preco_produto": preco.produto, "valor": valor, "preco_unidade": preco.unidade, "total": valortxt })
    }
    document.getElementById("valor" + i).textContent = valortxt
    total = total + (valor * preco.preco)

  }

  document.getElementById("listageral").value = JSON.stringify(listageral)

  if (document.getElementById("talheres").checked == true) {
    talher = 1;
    total = total + talher
  }

  document.getElementById("total").textContent = "R$ " + parseFloat(total).toFixed(2)
  document.getElementById("troco_separar").value = parseFloat(total).toFixed(2)
  document.getElementById("totalgeral").value = "R$ " + parseFloat(total).toFixed(2)
  console.log(document.getElementById("listageral").value)
}