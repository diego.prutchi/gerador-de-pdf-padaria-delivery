
const sobreController = require("../../controllers/sobre.controller");

module.exports = (routeV1) => {
    routeV1.route('/sobre')
        .get(sobreController.getSobre);
}