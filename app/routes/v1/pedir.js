const pedirController = require("../../controllers/pedir.controller");
const middlewareValidaDTO = require("../../utils/dto-validate");

module.exports = (routeV1) => {
  routeV1
    .route("/pedir")
    .get(pedirController.getPedir) // isso é uma conversa
    .post(middlewareValidaDTO("body", pedirController.postPedirSchema), pedirController.postPedir);
    // .post(curriculumController.postCurriculum);
};
