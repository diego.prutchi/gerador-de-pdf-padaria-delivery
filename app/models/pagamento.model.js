
const pagamentoDB = [
  {
    id: 1,
    descricao: "Dinheiro",
  },
  {
    id: 2,
    descricao: "Mastercard Débito",
  },
  {
    id: 3,
    descricao: "Mastercard Crédito",
  },
  {
    id: 4,
    descricao: "Visa Débito",
  },
  {
    id: 5,
    descricao: "Visa Crédito",
  },
  {
    id: 6,
    descricao: "Elo Débito",
  },
  {
    id: 7,
    descricao: "Elo Crédito",
  },
  {
    id: 8,
    descricao: "Amex Crédito",
  },
  {
    id: 9,
    descricao: "VR",
  },
];


const listaTodos = () => {

  return pagamentoDB;

}


const BuscaPorId = (id) => {

  const result = pagamentoDB.filter((item) => {
    return Number(item.id) === Number(id);
  });

  return result.length > 0 ? result[0] : undefined;

}


// const BuscaPorDescricao = (id) => {

//     const result = estadoCivilDB.filter((item) => {
//       return Number(item.id) === Number(id);
//     });

//     return result.length > 0 ? result[0] : undefined;

// }

// const criaNovoSexo = ({ id, descricao }) => {

//     // const result = sexoDB.filter((item) => {
//     //     return item.id === id;
//     // });

//     //todo: verificando se o id ja existe

//     //todo: verificando se a descricao ja existe



//     estadoCivilDB.push({ id, descricao });

// }


module.exports = {
  listaTodos,
  BuscaPorId,
  //criaNovoSexo
}