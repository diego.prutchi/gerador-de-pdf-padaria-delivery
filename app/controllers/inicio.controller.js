const getInicio = (req, res, next) => {
  const novotexto = Date();

  res.render("inicio", { novotexto });
};

module.exports = {
  getInicio,
};
